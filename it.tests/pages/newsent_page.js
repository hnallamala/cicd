'use strict';


var test = require('selenium-webdriver/testing');
var allureReporter = require('kp-webdriverjs-utils').allureReporter;
var createSignOnPage = require('kp-webdriverjs-utils').signOn;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var dataReader = require('kp-webdriverjs-utils').dataReader;
var helper = require('kp-webdriverjs-utils').helper;
var screenshot = require('kp-webdriverjs-utils/lib/screenshot');
var env = require('kp-webdriverjs-utils').env;
var locallinks = require('../data/environments').links;
var until = require('selenium-webdriver').until;
var locators = require('../data/locators');

var subnavLocators = require('../data/locators').subnav;
var inboxLocators = require('../data/locators').inbox;


module.exports = function(driver){

    return {
        VerifySentNoMessage: function () {
                driver.findElement(subnavLocators.sentLink).click().then(function(){
                driver.isElementPresent(inboxLocators.noMessage).then(function(isPresent) {
                expect(isPresent).to.be.true;
                });
                    driver.isElementPresent(inboxLocators.icon).then(function(isPresent) {
                expect(isPresent).to.be.true;
                });
             }); 
        }
    };
};


    
           


