var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;

module.exports = function(driver) {

	var help = {
		helpIcon: by.css(".icon-help"),
		helpContent: by.css(".help-content"),
	    provider: by.css('#select-provider'),
	    webManager:by.css('#select-web-manager'),
	    cancel: by.css("#cancel-compose-message"),
	    attachmentHelp: by.css("#attachments-help-link"),
	    goBack: by.css(".modal-buttons button.-cancel"),
	    verify: by.css(".modal-header"),
	    link: by.css("#emergency-message a"),
	    HelpText: by.css(".help-content h2"),
    	expectedHelpMessage: "Help with Messages",
    	composePage: by.css("#main-compose-button"),
    	EmergencyConfirm: by.css(".help-content h2"),
    	emergencyActual: "Emergency care and urgent care",
      imp: "Important",
      impId: by.css("#emergency-message strong")
	};

function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {

  	loadingPage: function() {
  		ready([
  			help.provider,
  			help.webManager
  		]);
  	},

  	verifyPage: function() {
  		ready([
  			help.helpContent
  		]);
  	},

  	verifyDoctorPage: function() {
  		ready([
  			help.cancel
  		]);
  	},

  	verifyAttachPage: function() {
  		ready([
  			help.verify
  		]);
  	},

  	getHelpPage:  function() {
  		return webdriver.promise.all([
  			driver.findElement(help.HelpText).getText()
  		]).then(function(results){
  			return {
  				content: results[0]
  			};
  		});
  	},

  	getEmergencyConfirm: function() {
  		return webdriver.promise.all([
  			driver.findElement(help.EmergencyConfirm).getText()
  		]).then(function(results) {
  			return {
  				content: results[0]
  			};
  		});
  	},

  	clickOnHelpIcon: function() {
  	   this.loadingPage();
  	   driver.findElement(help.helpIcon).click();
  	   this.verifyPage();
  	   var getContent = this.getHelpPage();
  	   return getContent;
  	},

  	clickHelpOnDoctorPage: function() {
  		this.loadingPage();
  		driver.findElement(help.provider).click();
  		this.verifyDoctorPage();
  		driver.findElement(help.helpIcon).click();
  		var getContent = this.getHelpPage();
  	   	return getContent;
  	},

  	clickOnAttachmentHelp: function() {
  		this.loadingPage();
  		driver.findElement(help.provider).click();
  		this.verifyDoctorPage();
  		driver.findElement(help.attachmentHelp).click();
  		this.verifyAttachPage();
  		driver.findElement(help.goBack).click();
  		this.verifyDoctorPage();
  	},

  	clickOnEmergencyHelp: function() {
  		this.loadingPage();
  		driver.findElement(help.provider).click();
  		this.verifyDoctorPage();
  		driver.findElement(help.link).click();
  		var getContent = this.getEmergencyConfirm();
  	   	return getContent;
  	},

    getRiskText: function() {
      return webdriver.promise.all([
        driver.findElement(help.impId).getText()
      ]).then(function(results) {
        return {
          content: results[0]
         };
      });
    },

    riskMitigationText: function() {
      this.loadingPage();
      driver.findElement(help.provider).click();
      this.verifyDoctorPage();
      var getContent = this.getRiskText();
      return getContent;
    },

  	verifyHelpPage: function(expectedContent) {
        return webdriver.promise.all([expectedContent]).then(function(results) {
          expect(results[0].content.toLowerCase()).to.equal(help.expectedHelpMessage.toLowerCase());
        });
      },

    verifyEmergencyHelp: function(expectedContent) {
    	return webdriver.promise.all([expectedContent]).then(function(results) {
    		expect(results[0].content.toLowerCase()).to.equal(help.emergencyActual.toLowerCase());
    	});
    },

    verifyRiskText: function(expectedContent) {
      return webdriver.promise.all([expectedContent]).then(function(results) {
        expect(results[0].content.toLowerCase()).to.equal(help.imp.toLowerCase());
      });
    }
  };
};
