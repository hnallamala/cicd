var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;
var helper = require('./helper');
var deletePage = require('./confirm_delete_page');
var allureReporter = require('kp-webdriverjs-utils/utils/allureReporter');

var inboxPageLocators = require('../data/locators').inbox;
var composePageLocators = require('../data/locators').compose;

module.exports = function(driver,syncTime) {
  var inbox = {
    /* Selectors used for May release scripts */
    messageFilter: by.css('#messages-filter .-value'),
    messageFooters: by.css('#messages-list .-footer'),
    filterProxy: by.css('#messages-filter .-options ul :nth-child(3) a'),
    senderName: by.css('.message-envelope .-footer'),
    messageSection: by.css('#message-section'),
    /* Selectors used for May release scripts */
    loadMore: by.css('#load-more-button'),
    to: by.css('.message .to'),
    from: by.css('.message .from'),
    selectedRegion: by.css('.custom-select-list .selected'),
    regionList: by.css('.custom-select-list li'),
    loadMoreEnabled : by.css("#load-more-button:enabled"),
    message: by.css('.message .message-metadata'),
    noMessageMessage: by.css('#no-messages-message'),
    secondFrom: by.css('.messages tr:nth-child(3) .from'),
    secondTo: by.css('.messages tr:nth-child(3) .to'),
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), syncTime);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      allureReporter.takeScreenshot("Inbox Page");
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  /*var awaitMessagesLoaded = function() {
    return helper.awaitCollection(function() {
      return driver.findElements(inbox.messages);
    });
  };*/

// Removing some of the selectors as part of new code base will update in future.

  return {
    verifyPage: function() {
      ready([
        by.css(inboxPageLocators.swapSelector),
        by.css(inboxPageLocators.composeButton)
      ]);
    },
    verifyMessages: function() {
      ready([
        by.css(inboxPageLocators.messages)
      ]);
    },
    verifyNoMessages: function() {
      ready([
        by.css(inboxPageLocators.noMessages)
      ]);
    },

    /*verifyRegionSelector: function(){
      ready([
        inbox.regionList
      ]);
    },
    verifyRegion:function(regionName){
      driver.findElement(inbox.selectedRegion).getText().then(function(name){
        expect(regionName).to.equal(name);
      });
    },
    getRegions: function(){
      var self = this;
      self.verifyPage();
      var homeRegionSelector = driver.findElement(inbox.selectedRegion).getText();
      self.clickHomeRegion();
      var regionListSelectors = driver.findElements(inbox.regionList);
      var regionsPromise = webdriver.promise.all([homeRegionSelector, regionListSelectors]).then(function(results) {
        var hostRegion = [];
        var homeRegionText = results[0];
        results[1].forEach(function(selector){
          selector.getText().then(function(host){
            if(homeRegionText!==host){
              hostRegion.push(host);
            }
          });
        });
        self.clickHomeRegion();
        return {
          homeRegion: homeRegionText,
          hostRegion: hostRegion
        };
      });
      return regionsPromise;
    },
    clickHomeRegion: function(){
      driver.findElement(inbox.selectedRegion).click();
    },
    selectRegion: function(region){
      this.verifyPage();
      this.verifyRegionSelector();

      this.clickHomeRegion();
      driver.wait(until.elementIsVisible(driver.findElement({name: region})), syncTime).then(function(result) {
        driver.findElement({name: region}).click();
      });
    },
    getMessageFilter: function(){
      return driver.findElement(inbox.messageFilter);
    },*/

    clickOnSent: function() {
      driver.wait(until.elementLocated(by.css(inboxPageLocators.successNotification)), env.timeOut).then(function() {
        allureReporter.takeScreenshot("Success message notification");
      });
      // I am adding all the wait times because all the funtionality is not developed. So Before DOM loads we have some locators that are present in the document. In that case if we are trying to do any event handling then it is doing it. Which is a bad way and almost all the times it will fail. So once development is completely finishes remove the wait time and verify the appro[riate locators.
      driver.sleep(3000);
      this.verifyPage();
      (by.css(inboxPageLocators.messages)) ? this.verifyMessages() : this.verifyNoMessages();
      driver.findElement(by.css(inboxPageLocators.swapSelector)).click();
      driver.findElement(by.css(inboxPageLocators.selectSent)).click();
      driver.sleep(3000);
      this.verifyMessages();
    },

    selectSent: function() {
      this.verifyPage();
      (by.css(inboxPageLocators.messages)) ? this.verifyMessages() : this.verifyNoMessages();
      driver.findElement(by.css(inboxPageLocators.swapSelector)).click();
      driver.findElement(by.css(inboxPageLocators.selectSent)).click();
      driver.sleep(3000);
      this.verifyMessages();
    },

    selectFirstMessageCheckBox: function() {
      this.verifyPage();
      // In case if the message is unread and you won't find any checkbox. So clicking on the twice.
      driver.findElement(by.css(inboxPageLocators.clickFirstMessageByHeading)).click();
      driver.sleep(3000);
      driver.findElement(by.css(inboxPageLocators.clickFirstMessageByHeading)).click();
      driver.findElement(by.css(inboxPageLocators.selectFirstMessage)).then(function(elem) {
        driver.actions().mouseMove(elem).perform();
        driver.sleep(5000);
        driver.findElement(by.css(inboxPageLocators.checkBox)).click().then(function() {
          allureReporter.takeScreenshot("Before clicking the checkbox");
          driver.findElement(by.css(inboxPageLocators.deleteButtonCheckBox)).click();
          var confirmDeletePage = deletePage(env.driver());
          confirmDeletePage.clickConfirmDelete();
        });
      });
    },

    clickOnTheFirstMessage: function() {
      this.verifyPage();
      this.verifyMessages();
      driver.findElement(by.css(inboxPageLocators.selectFirstMessage)).then(function(elem) {
        driver.actions().mouseMove(elem).perform();
        driver.findElement(by.css(inboxPageLocators.clickFirstMessageByHeading)).click();
        driver.sleep(3000);
        driver.findElement(by.css(inboxPageLocators.bodyMessage)).getText().then(function() {
          allureReporter.takeScreenshot();
        });
      });
      /*var firstMessageID = driver.findElement(inbox.messages).getAttribute('id').then(function(temp){
        return temp;
      });
      driver.sleep(5000);
      driver.wait(until.elementIsVisible(driver.findElement(inbox.bodyMessage)), syncTime);
      driver.wait(until.elementIsVisible(driver.findElement(inbox.deleteButton)), syncTime);
      driver.findElement(inbox.deleteButton).click().then(function(){
        // screenshot.take();
        var confirmDeletePage = deletePage(env.driver());
        confirmDeletePage.clickConfirmDelete();
      });*/
    },

    clickOnDeleteButton: function() {
      driver.findElement(by.css(inboxPageLocators.deleteButton)).click();
    },

    clickOnReplyLink: function() {
      driver.findElement(by.css(inboxPageLocators.replyLink)).click();
      driver.sleep(5000);
    },

    clickOnComposeButton: function() {
      this.verifyPage();
      (by.css(inboxPageLocators.messages)) ? this.verifyMessages() : this.verifyNoMessages();
      driver.findElement(by.css(inboxPageLocators.composeButton)).click();
    },
    verifyLoadMoreMessages: function() {
      this.verifyPage();
      driver.findElements(inbox.messages).then(function(messages) {
        expect(messages).to.have.length.within(0, 10);
      });
      var loadMoreButton = driver.wait(until.elementIsVisible(driver.findElement(inbox.loadMore)),syncTime);
      loadMoreButton.then(function(loadMoreButton) {
        // screenshot.take();
        expect(loadMoreButton).to.exist;
      });
      driver.findElement(inbox.loadMore).click().then(function() {
        // screenshot.take();
      });
      driver.wait(until.elementLocated(inbox.loadMoreEnabled),syncTime);
      driver.findElements(inbox.messages).then(function(messages) {
        // screenshot.take();
        expect(messages.length).to.be.at.least(11);
      });
    },
    verifyNoMoreMessages: function() {
      this.verifyPage();
      var emptyInbox = driver.findElement(inbox.emptyInbox);
      var noMessages = driver.findElement(inbox.noMessageMessage);
      webdriver.promise.all([emptyInbox, noMessages]).then(function(elements) {
        elements.forEach(function(element) {
          expect(element).to.exist;
        });
      }).thenCatch(function(err){
        assert(false, err);
      });
    },

    selectAProxy: function() {
      this.verifyPage();
      this.verifyMessages();
      driver.findElement(inbox.messageFilter).click();
      driver.findElement(inbox.filterProxy).click();
    },

    verifyAllMessagesAreForThatProxy: function() {
      this.verifyMessages();
      var expectedNamePromise = driver.findElement(inbox.messageFilter).getText();
      var proxieNamesPromise = helper.getTextListFor(inbox.senderName);

      webdriver.promise.all([proxieNamesPromise, expectedNamePromise]).then(function(result) {
        var expectedName = result[1].split("'s")[0].toLowerCase();
        var proxieNames = result[0].filter(function (proxy) {
          return proxy !== '';
        });

        proxieNames.forEach(function (proxy) {
          expect(proxy.toLowerCase()).to.contain(expectedName);
        });
      });
    }
  };
};
