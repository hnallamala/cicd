var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;
var allureReporter = require('kp-webdriverjs-utils/utils/allureReporter');

var deletePage = require('./confirm_delete_page');

module.exports = function(driver) {

  var sentList = {
    clickOnInbox: by.css('#messages-navigation'),
    clickOnSent: by.css('#messages-navigation :nth-child(2) #sent-option a'),
    listMessages: by.css('#messages-list'),
    messages: by.css('.messages .message'),
    header: by.css('#kp-header'),
    notification: by.css('#notification'),
    content: by.css('#content'),
    sent: by.css('#sent-link'),
    inbox:by.css('#inbox-link'),
    homeRegion: by.css('.custom-select-list .selected'),
    regionList: by.css('.custom-select-list li'),
    compose: by.css('#compose-button'),
    loadMore: by.css('#load-more-button'),
    sentNotification: by.css('.notification-buffer'),
    message: by.css('.message .message-metadata'),
    to: by.css('.message .to'),
    from: by.css('.message-metadata .from .name'),
    subject: by.css('.message .subject'),
    clickFirstMessage: by.css('.message .select-message'),
    delete: by.css('#delete-button'),
    loading: by.css('#notification .alert-loading'),
    alertMessage: by.css('#notification .alert-message'),
    messageFilter: by.css('#selected-filter'),
    successNotification: by.css('#notification .alert-success'),
    clickSecondMessage: by.css('.messages tr:nth-child(3) .select-message'),
    secondMessage: by.css('.messages tr:nth-child(3)')
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      allureReporter.takeScreenshot("Success message notification.");
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  var getFirstMessageDetails = function() {
    return webdriver.promise.all([
      driver.findElement(sentList.to).getText(),
      driver.findElement(sentList.from).getText(),
      driver.findElement(sentList.subject).getText()]).then(function(results) {
        return results;
      });
  };

  return {
    verifyPage: function() {
      ready([
        sentList.header,
        sentList.content,
        sentList.sent,
        sentList.compose
      ]);
    },

    verifyRegionSelector: function(){
      ready([
        sentList.regionList
      ]);
    },

    loadingPage: function() {
      ready([
        sentList.loading,
        sentList.alertMessage
      ]);
    },
    selectSentPage: function() {
      driver.wait(until.elementLocated(sentList.successNotification), env.timeOut).then(function() {
        allureReporter.takeScreenshot("Success message notification");
      });
      //driver.wait(until.elementIsVisible(sentList.listMessages), env.timeOut);
      //driver.findElement(sentList.clickOnInbox).click();
      /*driver.findElement(sentList.clickOnSent).click();
      driver.findElement(sentList.messages).getText().then(function() {
        screenshot.take();
      });*/
    },
    clickSentAfterCompose: function() {
      driver.wait(until.elementLocated(sentList.successNotification), env.timeOut);
      driver.wait(until.elementLocated(sentList.content), env.timeOut);
      driver.findElement(sentList.sent).click();
    },
    clickOnSentAndFirstMessage: function(expectedSubject) {
      this.clickSentAfterCompose();
      this.selectAndGetFirstMessageDetails();
    },

    getFirstMessageDetails :getFirstMessageDetails,

    clickHomeRegion: function(){
      driver.findElement(sentList.homeRegion).click();
    },
    getRegions: function(){
      var self = this;
      self.verifyPage();
      var homeRegionSelector = driver.findElement(sentList.homeRegion).getText();
      self.clickHomeRegion();
      var regionListSelectors = driver.findElements(sentList.regionList);
      var regionsPromise = webdriver.promise.all([homeRegionSelector, regionListSelectors]).then(function(results) {
        var hostRegion = [];
        var homeRegionText = results[0];
        results[1].forEach(function(selector){
          selector.getText().then(function(host){
            if(homeRegionText!==host){
              hostRegion.push(host);
            }
          });
        });
        self.clickHomeRegion();
        return {
          homeRegion: homeRegionText,
          hostRegion: hostRegion
        };
      });
      return regionsPromise;
    },
    selectRegion: function(region){
      this.verifyPage();
      this.verifyRegionSelector();

      this.clickHomeRegion();
      driver.wait(until.elementIsVisible(driver.findElement({name: region})), env.timeOut).then(function(result) {
        driver.findElement({name: region}).click();
      });
    },
    verifyMessageWasDeleted: function() {
      this.verifyPage();
      var firstMessageID = driver.findElement(sentList.messages).getAttribute('id').then(function(temp){
        return temp;
      });
      var secondMessageId = driver.findElement(sentList.secondMessage).getAttribute('id').then(function(temp) {
        return temp;
      });
      driver.findElement(sentList.clickFirstMessage).click();
      driver.findElement(sentList.clickSecondMessage).click();
      driver.findElement(sentList.delete).click().then(function(){
        var confirmDeletePage = deletePage(env.driver());
        confirmDeletePage.clickConfirmDelete();
      });
      this.loadingPage();
      driver.findElement(sentList.sent).click();
      this.verifyPage();
      var id = driver.findElements(sentList.messages).then(function(elements) {
        var textPromises =  elements.map(function(e) {
          return e.getAttribute('id');
        });
        return webdriver.promise.all(textPromises).then(function(text) {
          return text;
        });
      });
      webdriver.promise.all([id, firstMessageID, secondMessageId]).then(function(result) {
        for (var i=0; i < result[0].length; i++) {
          expect(result[0][i]).to.not.equal(result[1]);
          expect(result[0][i]).to.not.equal(result[2]);
        }
      });
    },
    selectAndGetFirstMessageDetails: function() {
      this.verifyPage();
      var messageText = getFirstMessageDetails();
      driver.findElement(sentList.message).click();
      return messageText;
    },
    verifyAllMessagesAreForThatProxy: function() {
      var expectedName = driver.findElement(sentList.messageFilter).getText();
      var fromName = helper.getTextListFor(sentList.from);
      webdriver.promise.all([fromName, expectedName]).then(function(result) {
        var fromNames = result[0];
        var expectedName = result[1];
        expect(fromNames.length).to.be.above(0);
        fromNames.forEach(function(name) {
          if (name!=='')
            expect(name).to.contain(expectedName);
        });
      });
    }
  };
};
