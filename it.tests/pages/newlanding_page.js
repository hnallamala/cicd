'use strict';


var test = require('selenium-webdriver/testing');
var allureReporter = require('kp-webdriverjs-utils').allureReporter;
var createSignOnPage = require('kp-webdriverjs-utils').signOn;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var dataReader = require('kp-webdriverjs-utils').dataReader;
var helper = require('kp-webdriverjs-utils').helper;
var screenshot = require('kp-webdriverjs-utils/lib/screenshot');
var env = require('kp-webdriverjs-utils').env;
var locallinks = require('../data/environments').links;
var locators = require('../data/locators');


module.exports = function(driver) {
    
    return {
        verifyIfHeaderIsPresent: function () {
            driver.isElementPresent(locators.landing.headerMenu).then(function (isPresent) {
                expect(isPresent).to.be.true;
            })
        },

        redirectToMCPage: function (region) {
            switch (region.toUpperCase()) {
                // Southern California
                case "SCAL":
                    helper.redirectUrl(locallinks.msgScalUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Northern California
                case "NCAL":
                    helper.redirectUrl(locallinks.msgNcalUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // denver colorado
                case "DBMN":
                    helper.redirectUrl(locallinks.msgColDBMNUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // south colorado
                case "COL":
                    helper.redirectUrl(locallinks.msgColUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Hawaii
                case "HI":
                    helper.redirectUrl(locallinks.msgHiUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Georgia
                case "GA":
                    helper.redirectUrl(locallinks.msgGAUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Mid atlantic
                case "MID":
                    helper.redirectUrl(locallinks.msgMidUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Northwest
                case "KNW":
                    helper.redirectUrl(locallinks.msgNWUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)
                    break;
                // Southern California
                default:
                    helper.redirectUrl(locallinks.msgScalUrl);
                    //helper.verifyTitle(locators.landing.messageCenterPageTitle)

            }
        },

        redirectToSpanishMCPage: function (region) {
            switch (region.toUpperCase()) {
                // Southern California
                case "SCAL":
                    helper.redirectUrl(locallinks.msgScalUrl);
                    break;
                // Northern California
                case "NCAL":
                    helper.redirectUrl(locallinks.msgNcalESUrl);
                    break;
                // denver colorado
                case "DBMN":
                    helper.redirectUrl(locallinks.msgColDBMNESUrl);
                    break;
                // south colorado
                case "SCO":
                    helper.redirectUrl(locallinks.msgColESUrl);
                    break;
                // Hawaii
                case "HI":
                    helper.redirectUrl(locallinks.msgHiESUrl);
                    break;
                // Georgia
                case "GA":
                    helper.redirectUrl(locallinks.msgGAESUrl);
                    break;
                // Mid atlantic
                case "MID":
                    helper.redirectUrl(locallinks.msgMidESUrl);
                    break;
                // Northwest
                case "KNW":
                    helper.redirectUrl(locallinks.msgNWESUrl);
                    break;
                // Southern California
                default:
                    helper.redirectUrl(locallinks.msgsScalESUrl);
            }
        }
    };

};