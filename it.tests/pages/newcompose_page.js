'use strict';

var test = require('selenium-webdriver/testing');
var allureReporter = require('kp-webdriverjs-utils').allureReporter;
var createSignOnPage = require('kp-webdriverjs-utils').signOn;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var dataReader = require('kp-webdriverjs-utils').dataReader;
var helper = require('kp-webdriverjs-utils').helper;
var screenshot = require('kp-webdriverjs-utils/lib/screenshot');
var env = require('kp-webdriverjs-utils').env;
var _ = require('underscore');
var locallinks = require('../data/environments').links;
var locators = require('../data/locators');

var subnavLocators = require('../data/locators').subnav;
var inboxLocators = require('../data/locators').inbox;
var composeLocators = require('../data/locators').compose;
var landingLocators = require('../data/locators').landing;

module.exports = function(driver){

return {

    clickComposeBtn: function(){
      driver.findElement(composeLocators.composebtn).click().then(function(){
        driver.isElementPresent(composeLocators.triagesection).then(function(isPresent){
          expect(isPresent).to.be.true;
        });
      });
    },

    verifyNoPcp: function(){
      driver.findElement(composeLocators.composebtn).click().then(function(){
        driver.isElementPresent(composeLocators.triagesection).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function(){
          driver.findElement(inboxLocators.cancel).click().then(function(){
          })
        })
        });
      });
    },

    verifyComposeMessageToDoctor: function(){
        driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function(){
         driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.fromCmd).then(function(isPresent){
           expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.toCmd).then(function(isPresent){
           expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.subjectCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.attachCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messagePostTextCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageSendCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageCancelCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
        })
        })

        })

        })

        })

        })

        })

        })

        });
    },

    verifySizeOfTilesInTriage: function(region) {

            if(region=="COL"){
                driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(6);
              });
              } else if(region=="HI"){
                driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(5);
              });
              } else if (region=="NCAL"){
              driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(4);
              });
              } else if (region=="GA") {
              driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(5);
              });
              } else if(region=="MID"){
              driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(6);
              });
              } else if(region=="SCAL"){
                driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(4);
              });
              } else if(region=="KNW"){
              driver.findElements(by.css('.icon-link-base.parbase.section')).then(function (count) {
                expect(count.length).to.equal(6);
              });
              }
    },

    cmdMsgValsubempty:function(){
      driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("need an appointment");
      driver.findElement(composeLocators.messageSendCmd).click();
      driver.sleep(500);
      driver.findElement(composeLocators.tooltipCloseBtn).click();
      driver.sleep(500);
    },

    cmdMsgValmsgempty:function(){
      driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("having back pain");
      driver.findElement(composeLocators.messageSendCmd).click();
        driver.sleep(500);
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.sleep(500);
    },

    cmdMsgValInvalidChar:function(){
      driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("@#$%⊠").then(function() {
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("@#$%⊠").then(function() {
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
       driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(composeLocators.tooltipOnSend).then(function(isPresent){
          expect(isPresent).to.be.true;
          driver.sleep(500);
        })
      });
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.sleep(500);
    },


    cmdMsgValSendSuccess:function(){
      driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("~`@#$%&*()_+=-}{|[]\":;'?><,./ASDasd123");
      driver.findElement(composeLocators.messageCmd).sendKeys("~`@#$%&*()_+=-}{|[]\":;'?><,./ASDasd123");
       driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(landingLocators.success).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
    },

    getTextOfElement: function (driver, locator) {
            return (driver.findElement(locator).getText().then(function(text) {
                return text;
            }));
    },

    assertTextOfElement: function (webElement, assertedText) {
        driver.findElement(webElement).getText().then(function (actualTextOnPage) {
            expect(actualTextOnPage).to.equal(assertedText);
            });
    },

    cmdMsgValCancel:function(){
      driver.findElement(composeLocators.doctorsOfficeIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("Testing Cancel Message");
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing Cancel Message");
       driver.findElement(composeLocators.messageCancelCmd).click().then(function() {
        driver.isElementPresent(composeLocators.cancelModalTitle).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
    },

    clickCancel: function(){
      driver.findElement(composeLocators.messageCancelCmd).click().then(function() {
        driver.isElementPresent(composeLocators.cancelModalTitle).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
    },

    clickYesCancel: function(){
      this.clickCancel();
      driver.sleep(500);
      driver.findElement(composeLocators.yesbtn).click().then(function() {
        driver.isElementPresent(subnavLocators.inboxLink).then(function(isPresent){
          expect(isPresent).to.be.true;
          driver.sleep(2000);
       })
      });
    },

    clickNoCancel: function(){
      driver.findElement(composeLocators.nobtn).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
          driver.sleep(2000);
       })
      });
    },

	//Advice Nurse

     verifyComposeMessageToAdviceNurse: function(){
        driver.findElement(composeLocators.adviceNurseIcon).click().then(function(){
        driver.isElementPresent(composeLocators.composeDoctorMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.fromCmd).then(function(isPresent){
           expect(isPresent).to.be.true;
         driver.findElement(composeLocators.toCmd).getText().then(function(actualText){
           expect(actualText).to.equal('Advice Nurse');
        driver.isElementPresent(composeLocators.subjectCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.attachCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messagePostTextCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageSendCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        driver.isElementPresent(composeLocators.messageCancelCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
        })

        })

        })

        })

        })

        })

        })
        })
        })
        });
    },

    adMsgValInvalidChar:function(){
      driver.findElement(composeLocators.adviceNurseIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeDoctorMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("@#$%⊠").then(function() {
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("@#$%⊠").then(function() {
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(composeLocators.tooltipOnSend).then(function(isPresent){
          expect(isPresent).to.be.true;
          driver.sleep(500);
        })
      });
      driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.sleep(500);
    },

    adMsgValmsgempty:function(){
      driver.findElement(composeLocators.adviceNurseIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeDoctorMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("having back pain");
      driver.findElement(composeLocators.messageSendCmd).click();
        driver.sleep(500);
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.sleep(500);
    },

    adMsgValsubempty:function(){
      driver.findElement(composeLocators.adviceNurseIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeDoctorMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("need an appointment");
      driver.findElement(composeLocators.messageSendCmd).click();
      driver.sleep(500);
      driver.findElement(composeLocators.tooltipCloseBtn).click();
      driver.sleep(500);
    },

    adMsgValCancel:function(){
      driver.findElement(composeLocators.adviceNurseIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeDoctorMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.subjectCmd).sendKeys("Testing Cancel Message");
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing Cancel Message");
       driver.findElement(composeLocators.messageCancelCmd).click().then(function() {
        driver.isElementPresent(composeLocators.cancelModalTitle).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
    },

    //Pharmacist

    verifyComposeMessageToPharmacist: function(){
        driver.findElement(composeLocators.pharmacistIcon).click().then(function(){
         driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
          });
        });
        driver.findElement(composeLocators.toCmd).getText().then(function(actualText){
           expect(actualText).to.equal('Pharmacist');
         });
        driver.isElementPresent(composeLocators.messageCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
          });
        driver.isElementPresent(composeLocators.messagePostTextCmd).then(function(isPresent){
          expect(isPresent).to.be.true;
          });
        driver.isElementPresent(composeLocators.pregnantchkbox).then(function(isPresent){
          expect(isPresent).to.be.true;
          });
        driver.isElementPresent(composeLocators.phonesection).then(function(isPresent){
          expect(isPresent).to.be.true;
          });
    },

    clickInfoLink: function(){
      driver.findElement(composeLocators.infolink).getText().then(function(){
        driver.findElement(composeLocators.infolink).click();
      });
    },

    cmpMsgValInvalidChar:function(){
      driver.findElement(composeLocators.pharmacistIcon).click().then(function(){
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("~`@#$&*+{}[]").then(function(){
        driver.findElement(composeLocators.messageSendCmd).click().then(function(){
        driver.isElementPresent(composeLocators.tooltipOnSend).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
        })
      });
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.findElement(composeLocators.messageCmd).sendKeys("Test Unsupported Character").then(function() {
        driver.findElement(composeLocators.medhist).sendKeys("~`&*()_+=-").then(function(){
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      })
      });
       driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(composeLocators.tooltipOnSend).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.findElement(composeLocators.messageCmd).sendKeys("Test Unsupported Character").then(function() {
        driver.findElement(composeLocators.currmed).sendKeys("~`&*()_+=-").then(function(){
        driver.isElementPresent(composeLocators.unsuppChar).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      })
      });
     },

    cmpMsgValCancel:function(){
      driver.findElement(composeLocators.pharmacistIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing Cancel Message");
       driver.findElement(composeLocators.messageCancelCmd).click().then(function() {
        driver.isElementPresent(composeLocators.cancelModalTitle).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      },

    cmpMsgValSendSuccess:function(){
      driver.findElement(composeLocators.pharmacistIcon).click().then(function() {
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing KANA Messages");
       driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(landingLocators.success).then(function(isPresent){
          expect(isPresent).to.be.true;

        })
      });
    },

    //WebManager

    clickWebMgr: function(){
        driver.findElement(composeLocators.wmIcon).click().then(function(){
         driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
          })
        });
    },


    verifyCMToWebMgr: function(){
        driver.findElement(composeLocators.wmIcon).click().then(function(){
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
        expect(isPresent).to.be.true;
        })
        });
        driver.findElement(composeLocators.wmFrom).getText().then(function(){
        driver.findElement(composeLocators.wmTo).getText().then(function(actualText){
           expect(actualText).to.include('Web Manager');
         })
         });
        driver.findElement(composeLocators.wmSub).getText().then(function(){
           console.log('The "actualText" paremeter used to be console logged here.');
         });
    },

    selectOptionFromDropDown: function(value){
      var dd= driver.findElement(composeLocators.wmTo);
      dd.findElements(by.tagName('option')).then(function(options){
        options.forEach(function(option){
          option.getText().then(function(text){
                if(text===value){
                  driver.sleep(2000);
               option.click();
              }
      
          });

        });

      });

    },

    selectOptionFromSubDropDown: function(value){
      var dd= driver.findElement(composeLocators.wmSub);
      return dd
      .findElements(by.tagName('option'))
      .then(function(options){
        return options[1].getText().then(function(text){
          if (text.trim() === value.trim()){
            options[1].click();
          }
        })
      });
    },


    cmWMMsgValCancel:function(){
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing Cancel Message");
       driver.findElement(composeLocators.messageCancelCmd).click().then(function() {
        driver.isElementPresent(composeLocators.cancelModalTitle).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
      },

    cmWMMsgValSendSuccess:function(){
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing KANA Messages");
       driver.findElement(composeLocators.messageSendCmd).click().then(function() {
        driver.isElementPresent(landingLocators.success).then(function(isPresent){
          expect(isPresent).to.be.true;
        })
      });
    },

    cmWMMsgValsubempty:function(){
      driver.findElement(composeLocators.messageCmd).sendKeys("Testing for Empty Subject");
      driver.findElement(composeLocators.messageSendCmd).click();
      driver.sleep(500);
      driver.findElement(composeLocators.tooltipCloseBtn).click();
      driver.sleep(500);
      driver.findElement(composeLocators.messageCmd).clear();
    },

    cmWMMsgValmsgempty:function(){
      driver.findElement(composeLocators.messageSendCmd).click();
        driver.sleep(500);
        driver.findElement(composeLocators.tooltipCloseBtn).click();
        driver.sleep(500);
    },

    // Member Services

    verifyCMToMemServices: function(){
        driver.findElement(composeLocators.msIcon).click().then(function(){
        driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
        expect(isPresent).to.be.true;
        })
        });
        driver.findElement(composeLocators.wmFrom).getText().then(function(){
        driver.findElement(composeLocators.wmTo).getText().then(function(actualText){
           expect(actualText).to.include('Member Services');
         })
         });
        driver.findElement(composeLocators.wmSub).getText().then(function(){
           console.log('The "actualText" paremeter used to be console logged here.');
         });
      },

    clickMemServices: function(){
        driver.findElement(composeLocators.msIcon).click().then(function(){
         driver.isElementPresent(composeLocators.composeMessage).then(function(isPresent){
          expect(isPresent).to.be.true;
          })
        });
    }
  };
};
