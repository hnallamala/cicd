var env = require('kp-webdriverjs-utils/support/env');
var expect = require('chai').expect;
var until = require('selenium-webdriver').until;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;


module.exports = function(driver) {

  var modal = {
  	title: by.css('#host-region-alert .modal-header'),
    confirmSwitch: by.css('#confirm-alert')
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {
    verifyPage: function() {
      ready([
      	modal.confirmSwitch
      ]);
    },
    verifyModalDetails: function() {
      this.verifyPage();
      expect(modal.title).to.exist;
    },
    clickConfirmSwitch: function() {
      this.verifyPage();
      driver.findElement(modal.confirmSwitch).click();
    }
  };
};
