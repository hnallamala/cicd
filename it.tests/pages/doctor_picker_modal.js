var env = require('kp-webdriverjs-utils/support/env');
var expect = require('chai').expect;
var until = require('selenium-webdriver').until;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;


module.exports = function(driver, form) {
  var testNG = (form === "desktop");

  var modal = {
  	title: by.css('#modal .modal-header'),
    closeIcon: by.css('#modal .modal-close'),
    closeButton: by.css('#cancel-provider-selection'),
    provider: by.css('#provider-options div.name'),
    providerDesktop: by.css('@provider-options button'),
    modalSelector: by.css('#from-options :nth-child(2)'),
    fromSelector: by.css('#from-options :nth-child(3)'),
    canceclSelection: by.css('#cancel-from-selection')

  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {
    verifyPage: function() {
      ready([
      	modal.title,
        modal.closeIcon,
        modal.closeButton
      ]);
    },
    verifyModalPage: function() {
      ready([
        modal.title,
        modal.closeIcon,
        modal.canceclSelection
      ]);
    },
    clickDoctor: function() {
      this.verifyPage();
      testNG ? driver.findElement(modal.providerDesktop).click() : driver.findElement(modal.provider).click();
    },
    clickSender: function() {
      this.verifyModalPage();
      driver.findElement(modal.modalSelector).click();
    },
    pickRightSender: function() {
      this.verifyModalPage();
      driver.findElement(modal.fromSelector).click();
    }
  };
};
