'use strict';

var test = require('selenium-webdriver/testing');
var allureReporter = require('kp-webdriverjs-utils').allureReporter;
var createSignOnPage = require('kp-webdriverjs-utils').signOn;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var dataReader = require('kp-webdriverjs-utils').dataReader;
var helper = require('kp-webdriverjs-utils').helper;
var screenshot = require('kp-webdriverjs-utils/lib/screenshot');
var env = require('kp-webdriverjs-utils').env;
var locallinks = require('../data/environments').links;
var until = require('selenium-webdriver').until;
var locators = require('../data/locators');

var subnavLocators = require('../data/locators').subnav;
var inboxLocators = require('../data/locators').inbox;


module.exports = function(driver){

    return {
        VerifyNoMessage: function () {
          driver.findElement(subnavLocators.inboxLink).click().then(function(){
          driver.isElementPresent(inboxLocators.noMessage).then(function(isPresent) {
              expect(isPresent).to.be.true;
              });
          driver.isElementPresent(inboxLocators.icon).then(function(isPresent) {
              expect(isPresent).to.be.true;
              });
          driver.isElementPresent(inboxLocators.composeLink).then(function(isPresent) {
              expect(isPresent).to.be.true;
              });
          driver.isElementPresent(inboxLocators.composeMsg).then(function(isPresent) {
              expect(isPresent).to.be.true;
              });
          });
        },

        VerifyPrint: function () {
          driver.isElementPresent(inboxLocators.print).then(function(isPresent) {
              expect(isPresent).to.be.true;
          });
        },
        VerifyDeleteMessage: function () {
            driver.isElementPresent(inboxLocators.deleteMessage).then(function(isPresent) {
                driver.findElement(inboxLocators.individualMessage).click().then(function(){
                    driver.findElement(inboxLocators.deleteMessage).isDisplayed().then(function(){
                        expect(isPresent).to.be.true;
                        console.log(inboxLocators.deleteMessage);
                        driver.findElement(inboxLocators.messageId).getAttribute("id").then(function(){
                            driver.findElement(inboxLocators.deleteMessage).click().then(function(){
                                driver.findElement(inboxLocators.confirmDelete).click().then(function(){
                                    driver.sleep(5000);
                                    driver.findElement(inboxLocators.messageId).getAttribute("id").then(function(){
                                        console.log('The "newvalue" used to be console logged here.');
                                    })
                                })
                            });
                        })
                    })
                });
            });
        },

        VerifyLoadMore: function () {
           driver.isElementPresent(inboxLocators.loadMore).then(function(isPresent) {
              expect(isPresent).to.be.true;
              var element = driver.findElement(inboxLocators.loadMoreBlock);
              driver.actions().mouseMove(element).perform();
              driver.findElement(inboxLocators.loadMore).click().then(function(){
              driver.sleep(5000);
              });
          });
        },
        VerifyLoadLess: function () {
            driver.wait(function(){
                driver.isElementPresent(inboxLocators.loadMore).then(function() {
                                var element = driver.findElement(inboxLocators.loadMoreBlock);
                                driver.actions().mouseMove(element).perform();
                                driver.findElement(inboxLocators.loadMore).click().then(function(){

                                })
                            })
                return driver.isElementPresent(inboxLocators.loadLess);
            },200000);
            driver.isElementPresent(inboxLocators.loadLess).then(function() {
                                var element = driver.findElement(inboxLocators.loadMoreBlock);
                                driver.actions().mouseMove(element).perform();
                                driver.findElement(inboxLocators.loadLess).click().then(function(){
                                    driver.sleep(10000);
                                })
                            })
        },
        VerifyBackToTop: function () {
                    driver.isElementPresent(inboxLocators.backToTop).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        var element = driver.findElement(inboxLocators.loadMoreBlock);
                        driver.actions().mouseMove(element).perform();
                        driver.findElement(inboxLocators.backToTop).click().then(function(){
                        driver.sleep(5000);
                        });
                    });
        },

        VerifyFirstTenMessages: function () {
                     driver.isElementPresent(inboxLocators.loadMore).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        driver.findElements(by.css('.message')).then(function(messagesLength){
                          expect(messagesLength.length).to.equal(10);
                        });
                    });
        },

        VerifyEmailExpanded: function(){
            driver.findElement(inboxLocators.messagetwo).click().then(function(){
                driver.isElementPresent(inboxLocators.messagetwo).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        console.log('Email expanded view is visible');
                    });
                driver.isElementPresent(inboxLocators.bodyTxt).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                });
                driver.isElementPresent(inboxLocators.date).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                });
                driver.isElementPresent(inboxLocators.recipient).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                });
                driver.isElementPresent(inboxLocators.print2).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        console.log('Print2 link is visible');
                });
                driver.isElementPresent(inboxLocators.delete2).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        console.log('Delete2 link is visible');
                });
                driver.isElementPresent(inboxLocators.reply2).then(function(isPresent) {
                        expect(isPresent).to.be.true;
                        console.log('Reply2 link is visible');
                });
            });
        }

    };
};
