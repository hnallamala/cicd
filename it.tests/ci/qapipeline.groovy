components = [
[project: 'kpweb', name: 'message-center', branch: 'master',job: [name :'message-center-smoke', test:"smoke", env:'dev20']]
];

repoRoot = 'ssh://git@bitbucket-fof.appl.kp.org:7999/'


def generateJobs(components) {
  components.each { component ->
    createJob component
  }
}

def createJob(component) {
    def gitRepo = repoRoot + component.project + '/' + component.name
    def gitBranch = 'master'
    if (component.branch) {
        gitBranch = component.branch
    }
    def timeTrigger = '@midnight'
    def testPattern = ~/\bsmoke*\b/
    def matcher = testPattern.matcher("${component.job.name}")

    if (matcher) {
       timeTrigger = 'H/15 * * * *'
    }


    job "kpweb-${component.job.name}-${component.job.env}", {
        authenticationToken('74d491f7bbbd6b32577f2cf599ddc3e6')
        logRotator numToKeep = 10
        wrappers {
          timeout {
            noActivity 180
          }
        }

        scm {
            git gitRepo, gitBranch, { node ->
                node / 'extensions' << 'hudson.plugins.git.extensions.impl.CleanCheckout'()
            }
        }

        authorization {
            permission('hudson.model.Item.Build', 'anonymous')
        }

        triggers {
           scm(timeTrigger)
        }

        steps {
            shell '''
            export PATH="$PATH:$(npm bin)"
            cd it.tests
            npm install
            rm -Rf allure-results*
            rm -Rf allure-reports*
            $(npm bin)/grunt test:endtoend --browser=ie-grid-firefox --reporter=mocha-multi --mochatestspecs=scripts/*_spec.js --grep='''+component.job.test +' --environment='+component.job.env
        }

        publishers {
             allure(['it.tests/allure-results'])

             archiveJunit('**/test-results.xml')
             wsCleanup {
                 includePattern('**/node_modules/**')
                 deleteDirectories(true)
             }
             mailer('kenneth.obikwelu@kp.org nikhita.mogili@kp.org', true, true)
        }
    }
}
generateJobs components
