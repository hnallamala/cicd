var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');

var by = webdriver.By;
var deletePage = require('./confirm_delete_page');

module.exports = function(driver,syncTime) {

  var details = {
    content:by.css('#content'),
    recipient: by.css('.recipient'),
    from: by.css('.details .from'),
    body: by.css('.body'),
    reply:by.css("#reply"),
    errorNotification: by.css('#notification .alert-error'),
    send: by.css('#send-message'),
    inbox: by.css(".inbox-text"),
    delete: by.css("#delete-button"),
    loading: by.css('#notification .alert-loading'),
    deleteNotification: by.css('#notification .alert-success'),
    openPrivacy: by.css('.more'),
    privacyViewed: by.css(".viewed"),
    privacyNotViewed: by.css(".not-viewed"),
    arrowDown: by.css(".icon-arrow-down"),
    arrowUp: by.css('.icon-arrow-up'),
    overlayLoading: by.css('#overlay .loading-content .icon-loading'),
    subject: by.css('.subject-bar .subject'),
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), syncTime);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {
    verifyPage: function() {
      ready([
        details.subject,
        details.recipient,
        details.from,
        details.body,
        details.content
      ]);
    },

    verifyLoading:function() {
      ready([
        details.overlayLoading
      ]);
    },

    verifyDelete: function() {
      ready([
        details.loading,
        details.deleteNotification
      ]);
    },

    clickReply: function() {
      return driver.findElement(details.reply).click();
    },

    verifyMessageWasDeleted: function(){
      var beforeDeleteURL = driver.getCurrentUrl();
      driver.findElement(details.delete).click().then(function() {
        var confirmDeletePage = deletePage(env.driver());
        confirmDeletePage.clickConfirmDelete();
      });
      this.verifyDelete();
      var afterDeleteURL = driver.getCurrentUrl();
      webdriver.promise.all([beforeDeleteURL, afterDeleteURL]).then(function(URL){
        expect(URL[0]).to.not.equal(URL[1]);
      });
      driver.get(beforeDeleteURL);
      driver.wait(until.elementLocated(details.content),syncTime);
      driver.findElement(details.errorNotification).getText().then(function(actualText){
        expect(actualText).to.equal('Failure to load message\nRETRY');
      });
    },

    getMessageDetails: function() {
      return webdriver.promise.all([
        driver.findElement(details.from).getText(),
        driver.findElement(details.subject).getText(),
        driver.findElement(details.body).getText()
      ]);
    },

    getPrivacyList: function() {
      driver.wait(until.elementIsVisible(driver.findElement(details.openPrivacy)),syncTime);
      driver.findElement(details.openPrivacy).click();
      return webdriver.promise.all([
        driver.findElement(details.privacyViewed).getText(),
        driver.findElement(details.privacyNotViewed).getText()
      ]);
    },

    clickOnDownArrow: function() {
      driver.findElement(details.arrowDown).click();
      driver.wait(until.elementLocated(by.css('#overlay .loading-content')), syncTime);
      driver.wait(until.elementLocated(by.css('#delete-button')),syncTime);
    },

    clickOnUpArrow: function() {
      driver.findElement(details.arrowUp).click();
      driver.wait(until.elementLocated(by.css('#overlay .loading-content')), syncTime);
      driver.wait(until.elementLocated(by.css('#delete-button')),syncTime);
    }
  };
};
