'use strict';


var test = require('selenium-webdriver/testing');
var allureReporter = require('kp-webdriverjs-utils').allureReporter;
var createSignOnPage = require('kp-webdriverjs-utils').signOn;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;
var _ = require('underscore');
var assert = require('chai').assert;
var dataReader = require('kp-webdriverjs-utils').dataReader;
var helper = require('kp-webdriverjs-utils').helper;
var screenshot = require('kp-webdriverjs-utils/lib/screenshot');
var env = require('kp-webdriverjs-utils').env;
var locallinks = require('../data/environments').links;
var until = require('selenium-webdriver').until;
var locators = require('../data/locators');

var subnavLocators = require('../data/locators').subnav;
var mcLocators = require('../data/locators').msgctr;


module.exports = function(driver){


    return {

        verifyGlobalHeaderPresentInMessageCenterPage: function () {
            driver.isElementPresent(subnavLocators.headerMenu).then(function (isPresent) {
                expect(isPresent).to.be.true;
            });
        },

        verifyInboxLink: function () {
                driver.findElement(subnavLocators.inboxLink).click().then(function(){
                    driver.sleep(2000);
                    driver.isElementPresent(subnavLocators.msgsection).then(function(isPresent) {
                expect(isPresent).to.be.true;
           });
          });
        },

        verifySentLink: function () {
            driver.findElement(subnavLocators.sentLink).click().then(function(){
            driver.sleep(2000);
            driver.isElementPresent(subnavLocators.msgsection).then(function(isPresent) {
                expect(isPresent).to.be.true;
           });
          });
        },

        verifyProxyPicker: function() {
            driver.isElementPresent(subnavLocators.proxypicker).then(function (isPresent) {
                expect(isPresent).to.be.true;
            });
        },

        selectProxyByName: function(name) {
            env.driver().wait(function () {
                return env.driver().isElementPresent({"css":"#proxy-members"});
            }, 20);
            env.driver().findElement({"css":"#proxy-members #proxy-picker-select-dropdown"}).click().then(function() {
                var proxyNameSelector = {"css":"#proxy-members #proxy-picker-select-dropdown"};
                var count  = 0;
                env.driver().findElements(proxyNameSelector).then(function(ele){
                    _.each(ele,function(el){
                        el.getText().then(function(x){
                            if(x == name) {
                                var sel = {"xpath":"//*[@id='proxy-picker-select-dropdown']/option[contains(text(),'"+name+"')]"};
                                env.driver().findElement(sel).click().then(function() {
                                    env.driver().findElement({"css":"#proxy-members #proxy-selector"}).getText().then(function(changedProxy){
                                        env.driver().sleep(5000);
                                        allureReporter.takeScreenshot('proxy name : '+name);
                                        assert.equal(changedProxy,name);
                                    });
                                });
                            }else{count=count+1}
                        });
                    })
                });
            });
        },

        switchToSpanish: function (){
            driver.findElement(mcLocators.headerSpanishLanguageButton).click().then(function(){
                driver.findElement(mcLocators.languageContinueButton).click();
                helper.verifyUrlContains(mcLocators.spanishLanguageText);
            });
        },


        displayproxies: function() {
            env.driver().wait(function () {
                return env.driver().isElementPresent({"css":"#proxy-members"});
            }, 20);
            env.driver().findElement({"css":"#proxy-members #proxy-picker-select-dropdown"}).click().then(function() {
                var proxyNameSelector = {"css":"#proxy-members #proxy-picker-select-dropdown"};
                var count  = 0;
                env.driver().findElements(proxyNameSelector).then(function(ele){
                    _.each(ele,function(el){
                        el.getText().then(function(x){
                            count=count+1
                        });
                    });
                });
            });
        }

    };

};
