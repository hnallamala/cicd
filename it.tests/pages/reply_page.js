var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;
var allureReporter = require('kp-webdriverjs-utils/utils/allureReporter');

var replyPageLocators = require("../data/locators").reply;
var composePageLocators = require('../data/locators').compose;


module.exports = function(driver) {

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      allureReporter.takeScreenshot("Reply Page");
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {

    verifyPage: function() {
      ready([
        by.css(replyPageLocators.from),
        by.css(replyPageLocators.to),
        by.css(replyPageLocators.subject)
      ]);
    },

    actualReplyUserInfo: function() {
      return webdriver.promise.all([
        driver.findElement(by.css(replyPageLocators.from)).getText(),
        driver.findElement(by.css(replyPageLocators.to)).getText(),
        driver.findElement(by.css(replyPageLocators.subject)).getText(),
        driver.findElement(by.css(composePageLocators.body)).getAttribute("value")
      ]);
    },

    enterMessageAndSubmit: function(randomString) {
      this.verifyPage();
      driver.findElement(by.css(composePageLocators.body)).sendKeys(randomString);
      var messageDetails = this.actualReplyUserInfo();
      driver.findElement(by.css(composePageLocators.sendButton)).click().then(function() {
        allureReporter.takeScreenshot("Replying for the message");
      });
      return messageDetails;
    }
  };
};
