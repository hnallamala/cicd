var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = require('selenium-webdriver').By;
var allureReporter = require('kp-webdriverjs-utils/utils/allureReporter');

var createMessagePrivacyPage = require('./message_privacy_page');
var createInboxPage = require('./inbox_page');
var createDoctorPickerModal = require('./doctor_picker_modal');

var composePageLocators = require('../data/locators').compose;
var modalLocators = require('../data/locators').modal;

module.exports = function(driver) {

// Remove all these selectors from here and place it in locators.json file and call from there as I did for some locators.
// I am adding wait time for some locators because all the funtionality is not developed. So Before DOM loads we have some locators that are present in the document. In that case if we are trying to do any event handling then it is doing it. Which is a bad way and almost all the times it will fail. So once development is completely finishes remove the wait time and verify the appro[riate locators.
  var compose = {
    title: by.css('.compose-message h2'),
    to: by.css('#to-value'),
    privacy: by.css('#privacy-picker-link'),
    attachment: by.css(".attachments-upload #add"),
    addAttachment: by.css(".info .is-attached"),
    phoneNumber: by.css("#primary-phone-field"),
    subjectPhone: by.css("#subject :nth-child(2)"),
    selectDoctor: by.css('#provider-options :nth-child(2)'),
    dayPhone: by.css("#primary-phone-field"),
    alternatePhone: by.css("#alternate-phone-field"),
    phoneNumberWeb: "(123)-456-7891",
    alternatePhoneNumber: "(123)-456-7890",
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector),env.seleniumtimeout);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      allureReporter.takeScreenshot("Compose Page");
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {
    verifyTriagePage: function() {
      ready([
        by.css(composePageLocators.triageHeading),
        by.css(composePageLocators.triageCancel),
        by.css(composePageLocators.triageHelp),
        by.css(composePageLocators.provider),
        by.css(composePageLocators.webManager)
      ]);
    },

    verifyOOOModal: function() {
      ready([
        by.css(modalLocators.OOOModal),
        by.css(modalLocators.OOOmodalButtons)
      ]);
    },

    verifyComposePage: function() {
      ready([
        by.css(composePageLocators.body),
        by.css(composePageLocators.sendButton),
        by.css(composePageLocators.composeCancel)
      ]);
    },

    verifyAttachment: function() {
      ready([
        compose.addAttachment
      ]);
    },

    verifyComposeTitle: function(title){
      ready([
        compose.title
      ]);
      driver.findElement(compose.title).getText().then(function(actualTitle){
        expect(actualTitle.toLowerCase()).to.contain(title);
      });
    },

    verifySend: function(){
      ready([
        by.css(composePageLocators.sendButton)
      ]);
    },

    getSubjectAndBody: function(message) {
        driver.findElement(by.css(composePageLocators.subjectField)).sendKeys(message.subject);
        driver.findElement(by.css(composePageLocators.body)).sendKeys(message.body);
    },

    clickSendButton: function() {
      driver.wait(until.elementIsVisible(driver.findElement(by.css(composePageLocators.sendButton)),env.seleniumtimeout)).then(function() {
        allureReporter.takeScreenshot("Clicking on send button");
        driver.findElement(by.css(composePageLocators.sendButton)).click();
      });
    },

    getMessageDetailsForMultipleUsers : function() {
      var composeDetails = this.actualComposeDetails(by.css(composePageLocators.selectFrom), by.css(composePageLocators.subjectField), by.css(composePageLocators.body));
      this.clickSendButton();
      return composeDetails;
    },

    getMessageDetailsForSingleUser: function() {
      var composeDetails = this.actualComposeDetails(by.css(composePageLocators.from), by.css(composePageLocators.subjectField), by.css(composePageLocators.body));
      this.clickSendButton();
      return composeDetails;
    },

    selectProvider: function() {
      driver.findElement(by.css(composePageLocators.selectProvider)).click();
      driver.wait(until.elementIsVisible(driver.findElement(by.css(composePageLocators.pickProvider))), env.seleniumtimeout).then(function() {
        allureReporter.takeScreenshot();
      });
      driver.findElement(by.css(composePageLocators.pickProvider)).click();
    },

    actualComposeDetails: function(from, subject, body) {
      return webdriver.promise.all([
        driver.findElement(from).getText(),
        driver.findElement(subject).getAttribute("value"),
        driver.findElement(body).getAttribute("value")
      ]);
    },
    actualComposeDetailsKana: function(from, to, subject, body) {
      return webdriver.promise.all([
        driver.findElement(from).getText(),
        driver.findElement(to).getText(),
        driver.findElement(subject).getText(),
        driver.findElement(body).getAttribute("value")
      ]);
    },

    composeToOutOfOfficeDoctor: function(message) {
      this.verifyTriagePage();
      driver.findElement(by.css(composePageLocators.provider)).click();
      this.verifyOOOModal();
      driver.findElement(by.css(modalLocators.OOOConfirmModal)).click();
      this.verifyComposePage();
      this.getSubjectAndBody(message);
      var composeDetails = this.actualComposeDetails(by.css(composePageLocators.from), by.css(composePageLocators.subjectField), by.css(composePageLocators.body));
      this.clickSendButton();
      return composeDetails;
    },

    composeMessageToDoctor: function(message) {
      this.verifyTriagePage();
      var self = this;
      driver.findElement(by.css(composePageLocators.provider)).click();
      return driver.findElement(by.css(composePageLocators.selectToContainer)).getText().then(function(result) {
        if(result === "Select") {
          self.selectProvider();
          /*  Need to write selecting a subject for Hawaii and MID users --> This users has subject as drop-down  */
          self.getSubjectAndBody(message);
      } else {
          self.getSubjectAndBody(message);
        }
      });
    },

    composeToDoctor: function(message){
      this.verifyComposePage();
      driver.findElement(compose.subject).sendKeys(message.subject);
      driver.findElement(compose.body).sendKeys(message.body);
      return this.actualComposeDetails(compose.from, compose.subject, compose.body);
    },

    composeForAdviceNurse: function(message) {
      this.verifyTriagePage();
      driver.findElement(by.css(composePageLocators.adviceNurse)).click();
      this.verifyComposePage();
      driver.findElement(by.css(composePageLocators.subjectField)).sendKeys(message.subject);
      driver.findElement(by.css(composePageLocators.body)).sendKeys(message.body);
      var composeDetails = this.actualComposeDetails(by.css(composePageLocators.selectFrom), by.css(composePageLocators.subjectField), by.css(composePageLocators.body));
      this.clickSendButton();
      return composeDetails;
    },

    composeToProgramMyCare: function(message) {
      this.verifyTriagePage();
      driver.findElement(by.css(composePageLocators.manageProgram)).click();
      this.verifyComposePage();
      driver.findElement(by.css(composePageLocators.selectToContainer)).click();
      driver.wait(until.elementIsVisible(driver.findElement(by.css(composePageLocators.selectWaitProgram))), env.seleniumtimeout);
      driver.findElement(by.css(composePageLocators.selectWaitProgram)).click();
      driver.findElement(by.css(composePageLocators.subjectField)).sendKeys(message.subject);
      driver.findElement(by.css(composePageLocators.body)).sendKeys(message.body);
      var composeDetails = this.actualComposeDetails(by.css(composePageLocators.selectFrom), by.css(composePageLocators.subjectField), by.css(composePageLocators.body));
      this.clickSendButton();
      return composeDetails;
    },

    composeToPharmacist: function(message) {
      this.verifyTriagePage();
      driver.findElement(by.css(composePageLocators.pharmacist)).click();
      this.verifyComposePage();
      driver.findElement(by.css(composePageLocators.body)).sendKeys(message.body);
      driver.findElement(by.css(composePageLocators.medicalHistory)).sendKeys(composePageLocators.pastMedicalHistory);
      driver.findElement(by.css(composePageLocators.currentMedications)).sendKeys(composePageLocators.currentTakingMedications);
      var getMessageDetails = this.actualComposeDetailsKana(by.css(composePageLocators.from), by.css(composePageLocators.selectTo), by.css(composePageLocators.currentMedications), by.css(composePageLocators.body));
      this.clickSendButton();
      return getMessageDetails;
    },

    composeForMemberServices: function(message){
      this.verifyTriagePage();
      driver.findElement(by.css(composePageLocators.memberServices)).click();
      this.verifyComposePage();
      driver.findElement(by.css(composePageLocators.webSubject)).click();
      driver.findElement(by.css(composePageLocators.selectSubject)).click();
      driver.findElement(by.css(composePageLocators.body)).sendKeys(message.body);
      var messageDetails =  this.actualComposeDetailsKana(by.css(composePageLocators.from), by.css(composePageLocators.selectTo), by.css(composePageLocators.selectedSubject), by.css(composePageLocators.body));
      this.clickSendButton();
      return messageDetails;
    },

    composeToWebManager: function(message){
      this.verifyPage();
      driver.findElement(compose.webManager).click();
      this.verifyComposePage();
      driver.findElement(compose.webSubject).click();
      driver.findElement(compose.body).sendKeys(message.body);
      var messageDetails = this.actualComposeDetails(compose.from, compose.subject, compose.body);
      this.clickSendButton();
      return messageDetails;
    },

    composeToDoctorWithAttachment: function(message) {
      this.verifyPage();
      driver.findElement(compose.provider).click();
      driver.findElement(compose.selectDoctor).click();
      driver.findElement(compose.subject).sendKeys(message.subject);
      driver.findElement(compose.attachment).click();
      this.verifyAttachment();
      driver.findElement(compose.body).sendKeys(message.body);
      var composeDetails = this.actualComposeDetailsForDoctorWithAttachments();
      this.clickSendButton();
      return composeDetails;
    },

    composeForDoctorWithPrivacyPicker: function(message) {
      this.verifyPage();
      driver.findElement(compose.provider).click();
      this.verifyComposePage();
      driver.findElement(compose.subject).sendKeys(message.subject);
      driver.findElement(compose.body).sendKeys(message.body);
      var composeDetailsForDoctor = this.actualComposeDetails(compose.selectFrom, compose.subject, compose.body);
      driver.findElement(compose.privacy).click();
      return composeDetailsForDoctor;
    },

    composeForProxy: function(message) {
      this.verifyTriagePage();
      var self = this;
      driver.findElement(by.css(composePageLocators.provider)).click();
      this.verifyComposePage();
      driver.findElement(by.css(composePageLocators.selectFrom)).click();
      driver.wait(until.elementIsVisible(driver.findElement(by.css(composePageLocators.selectProxyFromList))), env.seleniumtimeout);
      driver.findElement(by.css(composePageLocators.selectProxyFromList)).click().then(function(){
        allureReporter.takeScreenshot();
      });
      return driver.findElement(by.css(composePageLocators.selectToContainer)).getText().then(function(result) {
        if(result === "Select") {
          self.selectProvider();
        }
      self.getSubjectAndBody(message);
      });
    },

    selectUsersForPrivacy: function() {
      var privacyPickerPage = createMessagePrivacyPage(env.driver());
      var privacyDetails = privacyPickerPage.getPrivacyPickerNames();
      privacyPickerPage.setLastChild();
      privacyPickerPage.setPrivayPicker();
      var messageDetails = {};
      messageDetails = privacyDetails;
      this.clickSendButton();
      return messageDetails;
    },

    composeToWebManagerWithPhone: function(message){
      this.verifyPage();
      driver.findElement(compose.webManager).click();
      this.verifyComposePage();
      driver.findElement(compose.subjectPhone).click();
      driver.findElement(compose.body).sendKeys(message.body);
      driver.findElement(compose.phoneNumber).sendKeys(compose.phoneNumberWeb);
      var messageDetails = this.actualComposeDetailsKana(compose.from, compose.subject, compose.body, compose.dayPhone);
      this.clickSendButton();
      return messageDetails;
    }
  };
};
