"use strict"

var webdriverTestSuite = require("kp-webdriverjs-utils/webdriver.js");
module.exports = function(grunt) {
  var directories = {
    target: 'target'
  };
  webdriverTestSuite('endtoend', grunt, directories);
};