/*var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var webdriver = require('selenium-webdriver');
var expect = require('chai').expect;
var by = webdriver.By;
//var config = require('../support/'+ env.where +'.json');
var allureReporter = require('../support/allureReporter');

module.exports = function(driver, syncTime) {

  //var signonUrl = config.signonUrl;
  //var baseUrl = config.baseUrl;

  var signonUrl = "https://dev20.kaiserpermanente.org/content/kporg/en/national/sign-on.html#/";
  var baseUrl = "https://dev20.kaiserpermanente.org/content/kporg/en/national/mycare/my-health/message-center.html#/";

  var signOn = {
    username: by.css('[name=username]'),
    password: by.css('[name=password]'),
    signonForm: by.css('#signonform'),
    signonButton: by.css('#view5Submit')
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), 10000);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      //  screenshot.take();
       elements.forEach(function(element) {
        expect(element).to.exist;
      });
    }, function(err){
      console.log("Page not ready: ", err);
      throw err;
    });
  }

  return {
    verifyPage: function() {
      ready([
        signOn.username,
        signOn.password,
        signOn.signonForm
      ]);
    },
    signon: function(credentials) {
      var username = credentials.username;
      var password = credentials.password;
      console.log("username is: " + username);
      if (signonUrl){
        driver.get(signonUrl);
        console.log("signonUrl: ", signonUrl);
        // allureReporter.takeScreenshot('Before Signon screenshot');

        this.verifyPage();
        var signonForm = driver.findElement(signOn.signonForm);
        driver.findElement(signOn.username).sendKeys(username);
        driver.findElement(signOn.password).sendKeys(password);
        // allureReporter.takeScreenshot('After entering credentials screenshot');

        driver.findElement(signOn.signonButton).click().then(function(){
          driver.wait(until.stalenessOf(signonForm), 10000);
        }, function(err) {
          console.log("signonForm wait timeOut after: ", 10000)
          throw err;
        });
      }
      //driver.get(baseUrl);
    }
  };
};*/
