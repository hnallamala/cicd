var until = require('selenium-webdriver').until;
var expect = require('chai').expect;
var webdriver = require('selenium-webdriver');
var by = webdriver.By;

module.exports = function(driver,syncTime) {

  var landingPage = {
    inbox: by.linkText('Inbox'),
    sent: by.linkText('Sent'),
    compose: by.linkText('COMPOSE'),
    composeButton: by.css('#compose-button')
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), syncTime);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {
    verifyPage: function() {
      ready([
        landingPage.inbox,
        landingPage.sent,
        landingPage.compose,
        landingPage.composeButton
      ]);
    },
    selectInbox: function() {
      this.verifyPage();
      driver.findElement(landingPage.inbox).click();
    },
    selectSent: function() {
      this.verifyPage();
      driver.findElement(landingPage.sent).click();
    },
    selectCompose: function() {
      this.verifyPage();
      driver.findElement(landingPage.composeButton).click();
    }
  };
};
