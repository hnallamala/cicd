export PATH="$PATH:$(npm bin)"
cd it.tests
npm install
rm -Rf allure-results*
rm -Rf allure-reports*
$(npm bin)/grunt test:endtoend --browser=ie-grid-firefox --reporter=mocha-multi --mochatestspecs=scripts/*_spec.js --grep= --environment=$1
