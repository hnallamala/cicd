var env = require('kp-webdriverjs-utils/support/env');
var until = require('selenium-webdriver').until;
var webdriver = require('selenium-webdriver');
var expect = require('chai').expect;

var by = require('selenium-webdriver').By;

module.exports = function(driver) {

  var privacyPicker = {
    viewer: by.css('#viewer-options'),
    cancel: by.css('#cancel-viewers'),
    set: by.css('#set-viewers'),
    privacyList: by.css('.check-option'),
    confidentialCheck: by.css("ul#viewer-options .check-option:last-child > input")
  };

  function ready(selectors) {
    var waiters = selectors.map(function(selector) {
      return driver.wait(until.elementLocated(selector), env.timeOut);
    });

    webdriver.promise.all(waiters).then(function(elements) {
      screenshot.take();
      elements.forEach(function(element) {
        expect(element).to.exist;
      });
    });
  }

  return {

    verifyPage: function() {
      ready([
        privacyPicker.cancel,
        privacyPicker.set
      ]);
    },

    getPrivacyPickerNames: function() {
      this.verifyPage();
      return webdriver.promise.all([helper.getTextListFor(privacyPicker.privacyList)]).then(function(results) {
        return results[0];
      });
    },
    setLastChild: function() {
      driver.findElement(privacyPicker.confidentialCheck).click();
    },
    setPrivayPicker: function() {
      driver.findElement(privacyPicker.set).click();
    }
  };
};
